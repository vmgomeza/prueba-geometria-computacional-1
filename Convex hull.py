#Código envolvente convexa usado en la pregunta 2
from matplotlib import pyplot as plt
from random import randint
from math import *

#Retorna una lista de puntos (coordenadas) creados aleatoriamente (0 a 50)
def PuntosAlea(cant):
    return [[randint(0,16),randint(0,16)] for _ in range(cant)]

#Grafica los puntos y la envolvente convexa
def grafico(coords,convex_hull=None):
    x,y = zip(*coords) #Divide en dos listas (x,y)
    plt.scatter(x,y) #Grafica los puntos

    if convex_hull != None:
        for i in range(1, len(convex_hull) + 1):
            if i == len(convex_hull): i = 0
            c0 = convex_hull[i - 1]
            c1 = convex_hull[i]
            plt.plot((c0[0], c1[0]),(c0[1], c1[1]), 'r')

    plt.show()

#Devuelve el angulo entre p0 y p1(radianes)
def angulo(p0,p1=None):
    if p1==None: p1=pinicial
    disy=p0[1]-p1[1]
    disx=p0[0]-p1[0]
    return atan2(disy,disx)

# Devuelve la distancia entre p0 y p1
def distancia(p0,p1=None):
    if p1==None: p1=pinicial
    disy = p0[1] - p1[1]
    disx = p0[0] - p1[0]
    return sqrt(disy**2 + disx**2)

#Devuelve el determinante
# [p1(x) p1(y) 1]
#[p2(x) p2(y) 1]
# [p3(x) p3(y) 1]
# Si es >0 giro antihorario (izquierda)
# Si es <0 giro horario (derecha)
# Si es =0 colineal
def det(p1,p2,p3):
    return (p2[0]-p1[0])*(p3[1]-p1[1])-(p2[1]-p1[1])*(p3[0]-p1[0])

#Ordena en orden ascendiente los angulos, desde el punto inicial
def ListaOrdenada(a):
    if len(a)<=1: return a
    menor,igual,mayor=[],[],[]
    anginicial=angulo(a[randint(0,len(a)-1)]) #Elegimos un angulo inicial aleatorio
    for punto in a:
        ang_punto=angulo(punto) #calcula el angulo del punto
        if ang_punto<anginicial:  menor.append(punto)
        elif ang_punto==anginicial: igual.append(punto)
        else: 				  mayor.append(punto)
    return ListaOrdenada(menor) + sorted(igual,key=distancia) + ListaOrdenada(mayor)

#El metodo de Graham selecciona un punto inicial (menor valor)
#Ordena los puntos restantes en forma ascendiente segun su angulo
#Partiendo del punto inicial y siguendo la lista ordenada, comprueba cada 3 puntos
#si el giro es antihorario o horario, en funcion del signo se decide si borrar el punto central o no
def metodo_graham(puntos,progreso=False):
    global pinicial
    pminimo=None
    #Busca el punto con el minimo valor en (x,y)
    for i,(x,y) in enumerate(puntos):
        if pminimo==None or y<puntos[pminimo][1]:
            pminimo=i
        if y==puntos[pminimo][1] and x<puntos[pminimo][0]:
            pminimo=i
    pinicial=puntos[pminimo]

    #Ordena los puntos por sus angulos
    pordenados=ListaOrdenada(puntos)
    del pordenados[pordenados.index(pinicial)]
    #Crea la envolvente convexa a partir del determinante
    envolvente=[pinicial,pordenados[0]]
    for s in pordenados[1:]:
        while det(envolvente[-2],envolvente[-1],s)<=0:
            del envolvente[-1]
        envolvente.append(s)
        if progreso: grafico(puntos,envolvente)
    return envolvente

pts = [[-2, 72], [-71, 13], [17, -70], [9, 10], [-24, 67], [73, 60],
       [-37, -77], [54, 2], [78, -15],  [-41, -47],
       [-85, -25],  [70, -28]]
print ("Puntos:",pts)
envolvente=metodo_graham(pts,False)
print ("Envolvente convexa:",envolvente)
grafico(pts,envolvente)




